.. image:: https://gitlab.com/lpirl/lv2-nim/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/lv2-nim/pipelines
  :align: right



`Nim <https://nim-lang.org/>`__ bindings for the `LV2 audio plugin
specification <https://lv2plug.in/>`__.

To see an example of how to implement a LV2 plugin in Nim, please refer
to the `examples directory <examples>`__.

The author is not an expert in Nim, so suggestions regarding syntax,
semantics, and conventions are more than welcome.

`GPL 3.0 <https://opensource.org/licenses/GPL-3.0>`__ licensed.
