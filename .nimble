description = "Nim bindings for the LV2 plugin specification."
version = "0.1"
author = "Lukas Pirl"
license = "GPLv3"
