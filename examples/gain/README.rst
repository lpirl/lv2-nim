This is a small example of how to use the ``lv2-nim`` bindings.

`gain.nim <gain.nim>`__ demonstrates how to write LV2 plugins in Nim.
The ``ttl`` files contain no specifics, but are regular LV2 ``ttl``
files just as if the plugin would be written in, e.g., C.

To compile, execute::

  nim c --app:lib --noMain:on --gc:arc -d:release -o:gain.so gain.nim

To use this plugin with a LV2 host, set ``LV2_PATH`` accordingly,
e.g.::

  # lv2bm:
  LV2_PATH=$(readlink -f ..) lv2bm urn:lv2-nim:examples:gain

  # Ardour:
  LV2_PATH=$(readlink -f ..) Ardour6

  # ...
