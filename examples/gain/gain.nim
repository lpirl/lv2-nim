import
  math,
  ../../lv2-nim/lv2

type
  CSample = cfloat
  CSamples = UncheckedArray[CSample]
  PortIndex = enum
    INPUT = 0,
    OUTPUT = 1,
    GAIN = 2
  GainInstance = object
    input: ptr CSamples
    output: ptr CSamples
    gain: ptr cfloat


template db2coeff(db: cfloat): cfloat =
  pow(10, db / 20)


proc instantiate(descriptor: ptr Lv2Descriptor; sampleRate: cdouble;
                 bundlePath: cstring; features: ptr ptr Lv2Feature):
                Lv2Handle {.cdecl.} =
  return createShared(GainInstance)


proc connectPort(instance: Lv2Handle; port: cuint32;
                 dataLocation: pointer) {.cdecl.} =
  let gainInstance = cast[ptr GainInstance](instance)
  case cast[PortIndex](port)
  of INPUT:
    gainInstance.input = cast[ptr CSamples](dataLocation)
  of OUTPUT:
    gainInstance.output = cast[ptr CSamples](dataLocation)
  of GAIN:
    gainInstance.gain = cast[ptr cfloat](dataLocation)


proc activate(instance: Lv2Handle) {.cdecl.} =
  discard


proc run(instance: Lv2Handle; n_CSamples: cuint32) {.cdecl.} =
  let gainInstance = cast[ptr GainInstance](instance)
  for pos in 0 ..< n_CSamples:
    gainInstance.output[pos] =
      gainInstance.input[pos] * db2coeff(gainInstance.gain[])


proc deactivate(instance: Lv2Handle) {.cdecl.} =
  discard


proc cleanup(instance: Lv2Handle) {.cdecl.} =
  freeShared(cast[ptr GainInstance](instance))


proc extensionData(uri: cstring): pointer {.cdecl.} =
  return nil


proc NimMain*() {.importc.}
  ## This should be called once before anything else, probably in
  ## function ``lv2Descriptor`` (since external code, such as libraries,
  ## probably use GCed memory).


proc lv2Descriptor(index: cuint32): ptr Lv2Descriptor
                  {.cdecl, exportc, dynlib, extern: "lv2_descriptor".} =
  NimMain()
  case index
  of 0:
    result = createShared(Lv2Descriptor)
    result.uri = cstring("urn:lv2-nim:examples:gain")
    result.instantiate = instantiate
    result.connectPort = connectPort
    result.activate = activate
    result.run = run
    result.deactivate = deactivate
    result.cleanup = cleanup
    result.extensionData = extensionData
  else:
    result = nil
