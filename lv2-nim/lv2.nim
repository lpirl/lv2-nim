## Nim bindings for https://lv2plug.in/ by Lukas Pirl.
#
# Based on LV2 - An audio plugin interface specification.
# Copyright 2006-2012 Steve Harris, David Robillard.
#
# Based on LADSPA, Copyright 2000-2002 Richard W.E. Furse,
# Paul Barton-Davis, Stefan Westerfeld.
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


# Are you looking for (equivalents to) the LV2_CORE__* constants?
# Please visit https://gitlab.com/lpirl/lv2-nim/-/issues/1 .


type

  cuint32* {.importc: "unsigned int", nodecl.} = int

  Lv2Handle* = pointer
    ## Plugin Instance Handle.
    ##
    ## This is a handle for one particular instance of a plugin. It is
    ## valid to compare to NULL (or 0 for C++) but otherwise the host
    ## MUST NOT attempt to interpret it.

  Lv2Feature* {.pure.} = object
    ## Feature.
    ##
    ## Features allow hosts to make additional functionality available
    ## to plugins without requiring modification to the LV2 API.
    ## Extensions may define new features and specify the ``uri`` and
    ## ``data`` to be used if necessary. Some features, such as
    ## ``lv2:isLive``, do not require the host to pass data.

    uri*: cstring
      ## A globally unique, case-sensitive identifier (URI) for this
      ## feature.
      ##
      ## This MUST be a valid URI string as defined by RFC 3986.

    data*: pointer
      ## Pointer to arbitrary data.
      ##
      ## The format of this data is defined by the extension which
      ## describes the feature with the given `URI`.

  Lv2Descriptor* {.pure.} = object
    ## Plugin Descriptor.
    ##
    ## This objecture provides the core functions necessary to
    ## instantiate and use a plugin.

    uri*: cstring
      ## A globally unique, case-sensitive identifier for this plugin.
      ##
      ## This MUST be a valid URI string as defined by RFC 3986. All
      ## plugins with the same URI MUST be compatible to some degree,
      ## see http://lv2plug.in/ns/lv2core for details.

    instantiate*: proc (descriptor: ptr Lv2Descriptor;
                        sampleRate: cdouble; bundlePath: cstring;
                        features: ptr ptr Lv2Feature): Lv2Handle
                  {.cdecl.}
      ## Instantiate the plugin.
      ##
      ## Note that instance initialisation should generally occur in
      ## ``activate()`` rather than here. If a host calls instantiate(),
      ## it MUST call ``cleanup()`` at some point in the future.
      ##
      ## ``descriptor``: Descriptor of the plugin to instantiate.
      ##
      ## ``sampleRate``: Sample rate, in Hz, for the new plugin
      ## instance.
      ##
      ## ``bundlePath``: Path to the LV2 bundle which contains this
      ## plugin binary. It MUST include the trailing directory separator
      ## so that simply appending a filename will yield the path to that
      ## file in the bundle.
      ##
      ## ``features``: A NULL terminated array of ``Lv2Feature`` objects
      ## which represent the features the host supports. Plugins may
      ## refuse to instantiate if required features are not found here.
      ## However, hosts MUST NOT use this as a discovery mechanism:
      ## instead, use the RDF data to determine which features are
      ## required and do not attempt to instantiate unsupported plugins
      ## at all. This parameter MUST NOT be NULL, i.e. a host that
      ## supports no features MUST pass a single element array
      ## containing NULL.
      ##
      ## ``return``: A handle for the new plugin instance, or NULL if
      ## instantiation has failed.

    connectPort*: proc (instance: Lv2Handle; port: cuint32;
                         dataLocation: pointer) {.cdecl.}
      ## Connect a port on a plugin instance to a memory location.
      ##
      ## Plugin writers should be aware that the host may elect to use
      ## the same buffer for more than one port and even use the same
      ## buffer for both input and output (see ``lv2:inPlaceBroken`` in
      ## ``lv2.ttl``).
      ##
      ## If the plugin has the feature ``lv2:hardRTCapable`` then there
      ## are various things that the plugin MUST NOT do within the
      ## ``connectPort()`` function; see ``lv2core.ttl`` for details.
      ##
      ## ``connectPort()`` MUST be called at least once for each port
      ## before ``run()`` is called, unless that port is
      ## ``lv2:connectionOptional``. The plugin must pay careful
      ## attention to the block size passed to ``run()`` since the block
      ## allocated may only just be large enough to contain the data,
      ## and is not guaranteed to remain constant between ``run()``
      ## calls.
      ##
      ## ``connectPort()`` may be called more than once for a plugin
      ## instance to allow the host to change the buffers that the
      ## plugin is reading or writing. These calls may be made before or
      ## after ``activate()`` or ``deactivate()`` calls.
      ##
      ## ``instance``: Plugin instance containing the port.
      ##
      ## ``port``: Index of the port to connect. The host MUST NOT try
      ## to connect a port index that is not defined in the plugin's RDF
      ## data. If it does, the plugin's behaviour is undefined (a crash
      ## is likely).
      ##
      ## ``dataLocation``: Pointer to data of the type defined by the
      ## port type in the plugin's RDF data (for example, an array of
      ## float for an lv2:AudioPort). This pointer must be stored by the
      ## plugin instance and used to read/write data when ``run()`` is
      ## called. Data present at the time of the ``connectPort()`` call
      ## MUST NOT be considered meaningful.



    activate*: proc (instance: Lv2Handle) {.cdecl.}
      ## Initialise a plugin instance and activate it for use.
      ##
      ## This is separated from ``instantiate()`` to aid real-time
      ## support and so that hosts can reinitialise a plugin instance by
      ## calling ``deactivate()`` and then ``activate()``. In this case
      ## the plugin instance MUST reset all state information dependent
      ## on the history of the plugin instance except for any data
      ## locations provided by ``connectPort()``. If there is nothing
      ## for ``activate()`` to do then this field may be NULL.
      ##
      ## When present, hosts MUST call this function once before
      ## ``run()`` is called for the first time. This call SHOULD be
      ## made as close to the ``run()`` call as possible and indicates
      ## to real-time plugins that they are now live, however plugins
      ## MUST NOT rely on a prompt call to ``run()`` after activate().
      ##
      ## The host MUST NOT call ``activate()`` again until
      ## ``deactivate()`` has been called first. If a host calls
      ## ``activate()``, it MUST call ``deactivate()`` at some point in
      ## the future. Note that ``connectPort()`` may be called before or
      ## after ``activate()``.


    run*: proc (instance: Lv2Handle; sampleCount: cuint32) {.cdecl.}
      ## Run a plugin instance for a block.
      ##
      ## Note that if an ``activate()`` function exists then it must be
      ## called before ``run()``. If ``deactivate()`` is called for a
      ## plugin instance then ``run()`` may not be called until
      ## ``activate()`` has been called again.
      ##
      ## If the plugin has the feature ``lv2:hardRTCapable`` then there
      ## are various things that the plugin MUST NOT do within the
      ## ``run()`` function (see ``lv2core.ttl`` for details).
      ##
      ## As a special case, when ``sampleCount`` is 0, the plugin should
      ## update any output ports that represent a single instant in time
      ## (for example, control ports, but not audio ports). This is
      ## particularly useful for latent plugins, which should update
      ## their latency output port so hosts can pre-roll plugins to
      ## compute latency. Plugins MUST NOT crash when ``sampleCount`` is
      ## 0.
      ##
      ## ``instance``: Instance to be run.
      ##
      ## ``sampleCount``: The block size (in samples) for which
      ## the plugin instance must run.



    deactivate*: proc (instance: Lv2Handle) {.cdecl.}
      ## Deactivate a plugin instance (counterpart to ``activate()``).
      ##
      ## Hosts MUST deactivate all activated instances after they have
      ## been ``run()`` for the last time. This call SHOULD be made as
      ## close to the last ``run()`` call as possible and indicates to
      ## real-time plugins that they are no longer live, however plugins
      ## MUST NOT rely on prompt deactivation. If there is nothing for
      ## ``deactivate()`` to do then this field may be NULL
      ##
      ## Deactivation is not similar to pausing since the plugin
      ## instance will be reinitialised by ``activate()``. However,
      ## ``deactivate()`` itself MUST NOT fully reset plugin state. For
      ## example, the host may deactivate a plugin, then store its state
      ## (using some extension to do so).
      ##
      ## Hosts MUST NOT call ``deactivate()`` unless ``activate()`` was
      ## previously called. Note that ``connectPort()`` may be called
      ## before or after ``deactivate()``.


    cleanup*: proc (instance: Lv2Handle) {.cdecl.}
      ## Clean up a plugin instance (counterpart to ``instantiate()``).
      ##
      ## Once an instance of a plugin has been finished with it must be
      ## deleted using this function. The instance handle passed ceases
      ## to be valid after this call.
      ##
      ## If ``activate()`` was called for a plugin instance then a
      ## corresponding call to ``deactivate()`` MUST be made before
      ## ``cleanup()`` is called. Hosts MUST NOT call ``cleanup()``
      ## unless ``instantiate()`` was previously called.

    extensionData*: proc (uri: cstring): pointer {.cdecl.}
      ## Return additional plugin data defined by some extenion.
      ##
      ## A typical use of this facility is to return a object containing
      ## function pointers to extend the ``Lv2Descriptor`` API.
      ##
      ## The actual type and meaning of the returned object MUST be
      ## specified precisely by the extension. This function MUST return
      ## NULL for any unsupported URI. If a plugin does not support any
      ## extension data, this field may be NULL.
      ##
      ## The host is never responsible for freeing the returned value.


  lv2Descriptor* = proc (index: cuint32): ptr Lv2Descriptor {.cdecl.}
    ## Prototype for plugin accessor function.
    ##
    ## Plugins are discovered by hosts using RDF data (not by loading
    ## libraries). See http://lv2plug.in for details on the discovery
    ## process, though most hosts should use an existing library to
    ## implement this functionality.
    ##
    ## This is the simple plugin discovery API, suitable for most
    ## statically defined plugins. Advanced plugins that need access to
    ## their bundle during discovery can use lv2LibDescriptor() instead.
    ## Plugin libraries MUST include a function called "lv2Descriptor" or
    ## "Lv2LibDescriptor" with C-style linkage, but SHOULD provide
    ## "lv2Descriptor" wherever possible.
    ##
    ## When it is time to load a plugin (designated by its URI), the
    ## host loads the plugin's library, gets the ``lv2Descriptor()``
    ## function from it, and uses this function to find the
    ## ``Lv2Descriptor`` for the desired plugin. Plugins are accessed by
    ## index using values from 0 upwards. This function MUST return NULL
    ## for out of range indices, so the host can enumerate plugins by
    ## increasing ``index`` until NULL is returned.
    ##
    ## Note that ``index`` has no meaning, hosts MUST NOT depend on it
    ## remaining consistent between loads of the plugin library.


type
  lv2DescriptorFunction* = proc (index: cuint32): ptr Lv2Descriptor
                                {.cdecl.}
    ## Type of the ``lv2Descriptor()`` function in a library (old
    ## discovery API)

  Lv2LibHandle* = pointer
    ## Handle for a library descriptor.

  Lv2LibDescriptor* {.pure.} = object
    ## Descriptor for a plugin library.
    ##
    ## To access a plugin library, the host creates an
    ## ``Lv2LibDescriptor`` via the ``lv2LibDescriptor()`` function in
    ## the shared object.

    handle*: Lv2LibHandle
      ## Opaque library data which must be passed as the first parameter
      ## to all the methods of this object.

    size*: cuint32
      ## The total size of this object. This allows for this object to
      ## be expanded in the future if necessary. This MUST be set by
      ## the library to ``sizeof(Lv2LibDescriptor)``. The host MUST NOT
      ## access any fields of this object beyond ``get_plugin()`` unless
      ## this field indicates they are present.

    cleanup*: proc (handle: Lv2LibHandle) {.cdecl.}
      ## Destroy this library descriptor and free all related resources.

    get_plugin*: proc (handle: Lv2LibHandle; index: cuint32):
                      ptr Lv2Descriptor {.cdecl.}
      ## Plugin accessor.
      ##
      ## Plugins are accessed by index using values from 0 upwards. Out
      ## of range indices MUST result in this function returning NULL,
      ## so the host can enumerate plugins by increasing ``index`` until
      ## NULL is returned.

  lv2LibDescriptor* = proc (bundlePath: cstring;
                            features: ptr ptr Lv2Feature):
                           ptr Lv2LibDescriptor {.cdecl.}
    ## Prototype for library accessor function.
    ##
    ## This is the more advanced discovery API, which allows plugin
    ## libraries to access their bundles during discovery, which makes it
    ## possible for plugins to be dynamically defined by files in their
    ## bundle. This API also has an explicit cleanup function, removing
    ## any need for non-portable shared library deobjectors. Simple
    ## plugins that do not require these features may use
    ## ``lv2Descriptor()`` instead.
    ##
    ## This is the entry point for a plugin library. Hosts load this
    ## symbol from the library and call this function to obtain a library
    ## descriptor which can be used to access all the contained plugins.
    ## The returned object must not be destroyed (using
    ## ``Lv2LibDescriptor::cleanup()``) until all plugins loaded from
    ## that library have been destroyed.

  Lv2LibDescriptorFunction* = proc (bundlePath: cstring;
                                    features: ptr ptr Lv2Feature):
                                    ptr Lv2LibDescriptor {.cdecl.}
    ## Type of the ``lv2LibDescriptor()`` function in a LV2 library.
